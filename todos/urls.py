from django.urls import path
from todos.views import todo_list, show_todos


urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_todos, name="todo_list_detail"),
]
