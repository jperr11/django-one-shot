from django.shortcuts import render
from todos.models import ToDoList


# Create your views here.
def todo_list(request):
    todolist = ToDoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/list.html", context)


def show_todos(request, id):
    todolist = ToDoList.objects.get(id=id)
    context = {
        "todo_object": todolist,
    }
    return render(request, "todos/detail.html", context)
