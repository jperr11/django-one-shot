from django.db import models

# Create your models here.


class ToDoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)


class ToDoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )

    list = models.ForeignKey(
        ToDoList,
        related_name="items",
        on_delete=models.CASCADE,
    )

    is_completed = models.BooleanField(default=False)
